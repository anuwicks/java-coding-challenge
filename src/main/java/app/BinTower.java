package app;

import java.util.ArrayList;
import java.util.Collections;

public class BinTower {

    static final int maxHeight = 8; //const to hold maximum height of tower
    static final int bits = 8; // const to hold maximum length of a row

    private int[] allRows = new int[maxHeight]; //array to hold int values of input numbers
    private int currentRow = 0;

    public BinTower() {

    }

    public String addNewRow(int input) {
        //if input is 0, tower resets because no other levels can be added after
        if (input == 0) {
            allRows = new int[maxHeight];
            currentRow = 0;
            return this.createTower("A row of 0s has been added to the tower. Unable to create any more levels. Tower will now reset");
        }

        //if tower max height is reached the tower resets
        if (currentRow >= maxHeight) {
            allRows = new int[maxHeight];
            currentRow = 0;
            return this.createTower("Max tower height reached. Tower will now reset");
        }

        //if adding first row of tower assume row below is all 1s , if not first row check if 1s of new level correspond with 1s in previous level
        if (currentRow == 0 || this.checkColumns(input)){
            allRows[currentRow] = input;
            currentRow++;
            return this.createTower("Row added");
        } else {
            return this.createTower("Row not added;, Condition not met: 'A new level must only have 1s placed in it where a 1 exists in the corresponding column below it'");
        }

    }

    public String printTower(){
        return this.createTower("");
    }

    private String createTower(String message){
        String gridString;
        ArrayList<String> grid = new ArrayList<>();

        //convert row int values in to binary and pad to make 8 bits, then add padded binary values to arrayList for easier printing
        for (int row:allRows){
            String binary_val = String.format("%8s", Integer.toBinaryString(row)).replace(' ', '0');
            grid.add(binary_val);
        }
        //using an ArrayList makes it easier to add messages to grid.
        grid.add(message + "\n");
        //reverse ArrayList so tower can be printed bottom up
        Collections.reverse(grid);

        //format ArrayList by removing square braces and replace ", " with newline characters to make grid
        gridString = grid.toString().replaceAll("^.|.$", "").replace(", ", "\n");
        return gridString;
    }

    private Boolean checkColumns(int input){

        String previousRow = String.format("%8s", Integer.toBinaryString(allRows[currentRow-1])).replace(' ', '0');
        String newRow =   String.format("%8s", Integer.toBinaryString(input)).replace(' ', '0');

        //checks each character in both binary strings and returns false if new level has a column with 1 when the corresponding previous column does not
        for(int i = 0; i < bits; i++) {
            if (newRow.charAt(i) > previousRow.charAt(i)){
                return false;
            }
        }
        return true;
    }


}
