package app;

import java.util.Random;

import static java.lang.Integer.parseInt;
import static spark.Spark.*;

public class Application {

    public static void main(String[] args) {

        port(4567);
        BinTower tower = new BinTower();

        get("/ping", (req, res) -> {
           return "Hello, World!";
        });

        get("/view-tower", (req, res) -> {
            return tower.printTower();
        });

        post("/calc", (req, res) -> {
            String input = req.queryParams("input");
            if (input != null){
                //checks if input is a valid number.
                if(input.matches("-?\\d+(\\.\\d+)?")){
                    //checks if number is within the required range
                    if( parseInt(input) < 0 || parseInt(input) >255 ){
                        res.status(400);
                        return "Please enter a number between 0 & 255";
                    } else {
                        res.status(200);
                        return tower.addNewRow(parseInt(input));
                    }

                } else {
                    res.status(400);
                    return "Please enter a valid number";
                }
            } else {
                res.status(400);
                return "Please enter a valid x-www-form-urlencoded input ";
            }

        });



    }

}
